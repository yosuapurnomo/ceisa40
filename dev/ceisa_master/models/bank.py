
# -*- coding: utf-8 -*-
###############################################################################
#    License, author and contributors information in:                         #
#    __manifest__.py file at the root folder of this module.                  #
###############################################################################

from odoo import models, fields, api, _
from odoo.exceptions import UserError, ValidationError


class CeisaMasterBank(models.Model):
    _name = 'ceisa_master.bank'
    _description = 'Data Master Ceisa Bank'

    _rec_name = 'kode'
    _order = 'kode ASC'

    kode = fields.Char(
        string='Kode Bank',
        required=True
    )

    name = fields.Char(
        string='Nama Bank',
        required=True
    )

    active = fields.Boolean(
        string='Active',
        default=True
    )

    @api.model
    def create(self, vals_list):
        for vals in vals_list:
            vals['name'] = vals['name'].upper()
        return super(CeisaMasterBank, self).create(vals_list)

