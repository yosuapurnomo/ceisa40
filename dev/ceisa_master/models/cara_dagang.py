
# -*- coding: utf-8 -*-
###############################################################################
#    License, author and contributors information in:                         #
#    __manifest__.py file at the root folder of this module.                  #
###############################################################################

from odoo import models, fields, api, _
from odoo.exceptions import UserError, ValidationError


class CeisaMasterCaraDagang(models.Model):
    _name = 'ceisa_master.cara_dagang'
    _description = 'Data Master Ceisa Cara Dagang'

    _rec_name = 'kode'
    _order = 'kode ASC'

    kode = fields.Char(
        string='Kode Cara Dagang',
        required=True
    )

    name = fields.Char(
        string='Nama Cara Dagang',
        required=True,
    )

    active = fields.Boolean(
        string='Active',
        default=True
    )