
# -*- coding: utf-8 -*-
###############################################################################
#    License, author and contributors information in:                         #
#    __manifest__.py file at the root folder of this module.                  #
###############################################################################

from odoo import models, fields, api, _
from odoo.exceptions import UserError, ValidationError


class CeisaMasterPengusaha(models.Model):
    _name = 'ceisa_master.status_pengusaha'
    _description = 'Data Master Ceisa Status Pengusaha'

    _rec_name = 'uraian'
    _order = 'kode ASC'
    
    kode = fields.Char(
        string='Kode Status',
        required=True
    )
    
    uraian = fields.Char(
        string='Uraian Status Pengusaha',
        required=True,
    )

    active = fields.Boolean(
        string='Active',
        default=True
    )