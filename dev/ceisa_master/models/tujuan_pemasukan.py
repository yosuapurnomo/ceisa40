
# -*- coding: utf-8 -*-
###############################################################################
#    License, author and contributors information in:                         #
#    __manifest__.py file at the root folder of this module.                  #
###############################################################################

from odoo import models, fields, api, _
from odoo.exceptions import UserError, ValidationError


class CeisaMasterTujuanPemasukan(models.Model):
    _name = 'ceisa_master.tujuan_pemasukan'
    _description = 'Data Master Ceisa Tujuan Pemasukan'

    _rec_name = 'kode'
    _order = 'kode ASC'

    
    dokumen_id = fields.Many2one(
        string='Kode Dokumen',
        comodel_name='ceisa_master.dokumen',
        ondelete='cascade',
    )
    
    kode = fields.Char(
        string='Kode Tujuan Pemasukan',
        required=True
    )
    
    name = fields.Char(
        string='Nama Tujuan Pemasukan',
        required=True,
    )

    active = fields.Boolean(
        string='Active',
        default=True
    )

    @api.model
    def create(self, vals_list):
        for vals in vals_list:
            vals['name'] = vals['name'].upper()
        return super(CeisaMasterTujuanPemasukan, self).create(vals_list)