
# -*- coding: utf-8 -*-
###############################################################################
#    License, author and contributors information in:                         #
#    __manifest__.py file at the root folder of this module.                  #
###############################################################################

from odoo import models, fields, api, _
from odoo.exceptions import UserError, ValidationError


class CeisaMasterKategoriEkspor(models.Model):
    _name = 'ceisa_master.kategori_ekspor'
    _description = 'Data Master Ceisa Kategori Ekspor'

    _rec_name = 'kode'
    _order = 'kode ASC'
    
    kode = fields.Char(
        string='Kode Kategori Ekspor',
        required=True
    )
    
    name = fields.Char(
        string='Nama Kategori Ekspor',
        required=True,
    )

    active = fields.Boolean(
        string='Active',
        default=True
    )

