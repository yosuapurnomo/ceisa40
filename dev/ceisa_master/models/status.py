
# -*- coding: utf-8 -*-
###############################################################################
#    License, author and contributors information in:                         #
#    __manifest__.py file at the root folder of this module.                  #
###############################################################################

from odoo import models, fields, api, _
from odoo.exceptions import UserError, ValidationError


class CeisaMasterStatus(models.Model):
    _name = 'ceisa_master.status'
    _description = 'Data Master Ceisa Status'

    _rec_name = 'name'
    _order = 'kode ASC'
    
    kode = fields.Char(
        string='Kode Status',
        required=True
    )
    
    name = fields.Char(
        string='Nama Status',
        required=True,
    )

    active = fields.Boolean(
        string='Active',
        default=True
    )