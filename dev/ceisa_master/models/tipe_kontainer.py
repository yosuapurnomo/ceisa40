
# -*- coding: utf-8 -*-
###############################################################################
#    License, author and contributors information in:                         #
#    __manifest__.py file at the root folder of this module.                  #
###############################################################################

from odoo import models, fields, api, _
from odoo.exceptions import UserError, ValidationError


class CeisaMasterTipeKontainer(models.Model):
    _name = 'ceisa_master.tipe_kontainer'
    _description = 'Data Master Ceisa Tipe Kontainer'

    _rec_name = 'name'
    _order = 'kode ASC'
    
    kode = fields.Char(
        string='Kode Tipe Kontainer',
        required=True
    )
    
    name = fields.Char(
        string='Nama Tipe Kontainer',
        required=True,
    )

    active = fields.Boolean(
        string='Active',
        default=True
    )