
# -*- coding: utf-8 -*-
###############################################################################
#    License, author and contributors information in:                         #
#    __manifest__.py file at the root folder of this module.                  #
###############################################################################

from odoo import models, fields, api, _
from odoo.exceptions import UserError, ValidationError


class CeisaMasterTransaksiDagang(models.Model):
    _name = 'ceisa_master.transaksi_dagang'
    _description = 'Data Master Ceisa Jenis Transaksi Dagang'

    _rec_name = 'kode'
    _order = 'kode ASC'
    
    kode = fields.Char(
        string='Kode Jenis Transaksi Dagang',
        required=True
    )
    
    name = fields.Char(
        string='Nama Jenis Transaksi Dagang',
        required=True,
    )

    active = fields.Boolean(
        string='Active',
        default=True
    )

    @api.model
    def create(self, vals_list):
        for vals in vals_list:
            vals['kode'] = vals['kode'].upper()
        return super(CeisaMasterTransaksiDagang, self).create(vals_list)