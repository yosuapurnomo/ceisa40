
# -*- coding: utf-8 -*-
###############################################################################
#    License, author and contributors information in:                         #
#    __manifest__.py file at the root folder of this module.                  #
###############################################################################

from odoo import models, fields, api, _
from odoo.exceptions import UserError, ValidationError


class CeisaMasterSatuanBarang(models.Model):
    _name = 'ceisa_master.satuan_barang'
    _description = 'Data Master Ceisa Satuan Barang'

    _rec_name = 'name'
    _order = 'kode ASC'
    
    kode = fields.Char(
        string='Kode Satuan Barang',
        required=True
    )
    
    name = fields.Char(
        string='Nama Satuan Barang',
        required=True,
    )

    active = fields.Boolean(
        string='Active',
        default=True
    )