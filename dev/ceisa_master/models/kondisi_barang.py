
# -*- coding: utf-8 -*-
###############################################################################
#    License, author and contributors information in:                         #
#    __manifest__.py file at the root folder of this module.                  #
###############################################################################

from odoo import models, fields, api, _
from odoo.exceptions import UserError, ValidationError


class CeisaMasterKondisiBarang(models.Model):
    _name = 'ceisa_master.kondisi_barang'
    _description = 'Data Master Ceisa Kondisi Barang'

    _rec_name = 'kode'
    _order = 'kode ASC'
    
    kode = fields.Char(
        string='Kode Kondisi Barang',
        required=True
    )
    
    name = fields.Char(
        string='Nama Kondisi Barang',
        required=True,
    )

    active = fields.Boolean(
        string='Active',
        default=True
    )
