
# -*- coding: utf-8 -*-
###############################################################################
#    License, author and contributors information in:                         #
#    __manifest__.py file at the root folder of this module.                  #
###############################################################################

from odoo import models, fields, api, _
from odoo.exceptions import UserError, ValidationError


class CeisaMasterNegara(models.Model):
    _name = 'ceisa_master.negara'
    _description = 'Data Master Ceisa Negara'

    _rec_name = 'name'
    _order = 'kode ASC'
    
    kode = fields.Char(
        string='Kode Negara',
        required=True
    )
    
    name = fields.Char(
        string='Nama Negara',
        required=True,
    )

    active = fields.Boolean(
        string='Active',
        default=True
    )

    @api.model
    def create(self, vals_list):
        for vals in vals_list:
            vals['kode'] = vals['kode'].upper()
            vals['name'] = vals['name'].upper()
        return super(CeisaMasterNegara, self).create(vals_list)