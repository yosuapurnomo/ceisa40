
# -*- coding: utf-8 -*-
###############################################################################
#    License, author and contributors information in:                         #
#    __manifest__.py file at the root folder of this module.                  #
###############################################################################

from odoo import models, fields, api, _
from odoo.exceptions import UserError, ValidationError


class CeisaMasterDaerahAsal(models.Model):
    _name = 'ceisa_master.daerah_asal'
    _description = 'Data Master Ceisa Daerah Asal'

    _rec_name = 'kode'
    _order = 'kode ASC'

    
    kode = fields.Char(
        string='Kode Daerah Asal',
        required=True
    )
    
    name = fields.Char(
        string='Nama Daerah Asal',
        required=True,
    )

    active = fields.Boolean(
        string='Active',
        default=True
    )