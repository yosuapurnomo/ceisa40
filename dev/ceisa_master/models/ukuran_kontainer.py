
# -*- coding: utf-8 -*-
###############################################################################
#    License, author and contributors information in:                         #
#    __manifest__.py file at the root folder of this module.                  #
###############################################################################

from odoo import models, fields, api, _
from odoo.exceptions import UserError, ValidationError


class CeisaMasterUkuranKontainer(models.Model):
    _name = 'ceisa_master.ukuran_kontainer'
    _description = 'Data Master Ceisa Ukuran Kontainer'

    _rec_name = 'kode'
    _order = 'kode ASC'
    
    kode = fields.Char(
        string='Kode Ukuran Kontainer',
        required=True
    )
    
    name = fields.Char(
        string='Nama Ukuran Kontainer',
        required=True,
    )

    active = fields.Boolean(
        string='Active',
        default=True
    )

    @api.model
    def create(self, vals_list):
        for vals in vals_list:
            vals['name'] = vals['name'].upper()
        return super(CeisaMasterUkuranKontainer, self).create(vals_list)