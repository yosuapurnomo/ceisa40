
# -*- coding: utf-8 -*-
###############################################################################
#    License, author and contributors information in:                         #
#    __manifest__.py file at the root folder of this module.                  #
###############################################################################

from odoo import models, fields, api, _
from odoo.exceptions import UserError, ValidationError


class CeisaMasterKhusus(models.Model):
    _name = 'ceisa_master.khusus'
    _description = 'Data Master Ceisa Spesifikasi Khusus'

    _rec_name = 'name'
    _order = 'kode ASC'
    
    kode = fields.Char(
        string='Kode Spesifikasi Khusus',
        required=True
    )
    
    name = fields.Char(
        string='Nama Spesifikasi Khusus',
        required=True,
    )

    active = fields.Boolean(
        string='Active',
        default=True
    )

    @api.model
    def create(self, vals_list):
        for vals in vals_list:
            vals['name'] = vals['name'].upper()
        return super(CeisaMasterKhusus, self).create(vals_list)