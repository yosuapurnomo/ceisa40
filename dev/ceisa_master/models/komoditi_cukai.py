
# -*- coding: utf-8 -*-
###############################################################################
#    License, author and contributors information in:                         #
#    __manifest__.py file at the root folder of this module.                  #
###############################################################################

from odoo import models, fields, api, _
from odoo.exceptions import UserError, ValidationError


class CeisaMasterKomoditiCukai(models.Model):
    _name = 'ceisa_master.komoditi_cukai'
    _description = 'Data Master Ceisa Komoditi Cukai'

    _rec_name = 'kode'
    _order = 'kode ASC'
    
    kode = fields.Char(
        string='Kode Komoditi Cukai',
        required=True
    )
    
    name = fields.Char(
        string='Nama Komoditi Cukai',
        required=True,
    )

    active = fields.Boolean(
        string='Active',
        default=True
    )

