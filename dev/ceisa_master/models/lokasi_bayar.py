
# -*- coding: utf-8 -*-
###############################################################################
#    License, author and contributors information in:                         #
#    __manifest__.py file at the root folder of this module.                  #
###############################################################################

from odoo import models, fields, api, _
from odoo.exceptions import UserError, ValidationError


class CeisaMasterLokasiBayar(models.Model):
    _name = 'ceisa_master.lokasi_bayar'
    _description = 'Data Master Ceisa Lokasi Bayar'

    _rec_name = 'kode'
    _order = 'kode ASC'
    
    kode = fields.Char(
        string='Kode Lokasi Bayar',
        required=True
    )
    
    name = fields.Char(
        string='Nama Lokasi Bayar',
        required=True,
    )

    active = fields.Boolean(
        string='Active',
        default=True
    )

    @api.model
    def create(self, vals_list):
        for vals in vals_list:
            vals['name'] = vals['name'].upper()
        return super(CeisaMasterLokasiBayar, self).create(vals_list)