
# -*- coding: utf-8 -*-
###############################################################################
#    License, author and contributors information in:                         #
#    __manifest__.py file at the root folder of this module.                  #
###############################################################################

from odoo import models, fields, api, _
from odoo.exceptions import UserError, ValidationError


class CeisaMasterFasilitas(models.Model):
    _name = 'ceisa_master.fasilitas'
    _description = 'Data Master Ceisa Fasilitas'

    _rec_name = 'kode'
    _order = 'kode ASC'
    
    kode = fields.Char(
        string='Kode Fasilitas',
        required=True
    )
    
    name = fields.Char(
        string='Nama Fasilitas',
        required=True,
    )

    active = fields.Boolean(
        string='Active',
        default=True
    )