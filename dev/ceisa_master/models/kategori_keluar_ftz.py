
# -*- coding: utf-8 -*-
###############################################################################
#    License, author and contributors information in:                         #
#    __manifest__.py file at the root folder of this module.                  #
###############################################################################

from odoo import models, fields, api, _
from odoo.exceptions import UserError, ValidationError


class CeisaMasterKategoriKeluar(models.Model):
    _name = 'ceisa_master.kategori_keluar'
    _description = 'Data Master Ceisa Kategori Keluar Dokumen FTZ'

    _rec_name = 'kode'
    _order = 'kode ASC'
    
    kode = fields.Char(
        string='Kode Kategori Keluar FTZ',
        required=True
    )
    
    uraian = fields.Char(
        string='Uraian Kategori Keluar FTZ',
        required=True,
    )

    active = fields.Boolean(
        string='Active',
        default=True
    )
