
# -*- coding: utf-8 -*-
###############################################################################
#    License, author and contributors information in:                         #
#    __manifest__.py file at the root folder of this module.                  #
###############################################################################

from odoo import models, fields, api
from odoo.exceptions import UserError, ValidationError


class CeisaMasterValuta(models.Model):
    _name = 'ceisa_master.valuta'
    _description = 'Data Master Ceisa Valuta'

    _rec_name = 'kode'
    _order = 'kode ASC'
    
    kode = fields.Char(
        string='Kode Valuta',
        required=True
    )
    
    name = fields.Char(
        string='Nama Valuta',
        required=True,
    )

    active = fields.Boolean(
        string='Active',
        default=True
    )

    @api.model
    def create(self, vals_list):
        for vals in vals_list:
            vals['kode'] = vals['kode'].upper()
        return super(CeisaMasterValuta, self).create(vals_list)