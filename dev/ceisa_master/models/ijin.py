
# -*- coding: utf-8 -*-
###############################################################################
#    License, author and contributors information in:                         #
#    __manifest__.py file at the root folder of this module.                  #
###############################################################################

from odoo import models, fields, api, _
from odoo.exceptions import UserError, ValidationError


class CeisaMasterIjin(models.Model):
    _name = 'ceisa_master.ijin'
    _description = 'Data Master Ceisa Ijin'

    _rec_name = 'kode'
    _order = 'kode ASC'
    
    kode = fields.Char(
        string='Kode Ijin',
        required=True
    )
    
    name = fields.Char(
        string='Nama Ijin',
        required=True,
    )

    active = fields.Boolean(
        string='Active',
        default=True
    )