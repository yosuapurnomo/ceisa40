from odoo import api, fields, models


class CeisaMasterAsalBarang(models.Model):
    _name = 'ceisa_master.asal_barang'
    _description = 'Data Master Ceisa Asal Barang'

    _rec_name = 'kode'
    _order = 'kode ASC'

    kode = fields.Char(
        string='Kode Asal Barang',
        required=True,
    )
    name = fields.Char(
        string='Nama Asal Barang',
        required=True
    )

    active = fields.Boolean(
        string='Active',
        default=True
    )
    
    

    
