
# -*- coding: utf-8 -*-
###############################################################################
#    License, author and contributors information in:                         #
#    __manifest__.py file at the root folder of this module.                  #
###############################################################################

from odoo import models, fields, api, _
from odoo.exceptions import UserError, ValidationError


class CeisaMasterRespon(models.Model):
    _name = 'ceisa_master.respon'
    _description = 'Data Master Ceisa Respon'

    _rec_name = 'kode'
    _order = 'kode ASC'
    
    kode = fields.Char(
        string='Kode Respon',
        required=True
    )
    
    uraian = fields.Char(
        string='Uraian Respon',
        required=True,
    )

    active = fields.Boolean(
        string='Active',
        default=True
    )

    @api.model
    def create(self, vals_list):
        for vals in vals_list:
            vals['kode'] = vals['kode'].upper()
            vals['name'] = vals['name'].upper()
        return super(CeisaMasterRespon, self).create(vals_list)