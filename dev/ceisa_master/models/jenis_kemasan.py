
# -*- coding: utf-8 -*-
###############################################################################
#    License, author and contributors information in:                         #
#    __manifest__.py file at the root folder of this module.                  #
###############################################################################

from odoo import models, fields, api, _
from odoo.exceptions import UserError, ValidationError


class CeisaMasterKemasan(models.Model):
    _name = 'ceisa_master.kemasan'
    _description = 'Data Master Ceisa Kemasan'

    _rec_name = 'kode'
    _order = 'kode ASC'
    
    kode = fields.Char(
        string='Kode Jenis Kemasan',
        required=True
    )
    
    name = fields.Char(
        string='Nama Jenis Kemasan',
        required=True,
    )

    active = fields.Boolean(
        string='Active',
        default=True
    )

    @api.model
    def create(self, vals_list):
        for vals in vals_list:
            vals['kode'] = vals['kode'].upper()
        return super(CeisaMasterKemasan, self).create(vals_list)