
# -*- coding: utf-8 -*-
###############################################################################
#    License, author and contributors information in:                         #
#    __manifest__.py file at the root folder of this module.                  #
###############################################################################

from odoo import models, fields, api, _
from odoo.exceptions import UserError, ValidationError


class CeisaMasterCaraAngkut(models.Model):
    _name = 'ceisa_master.cara_angkut'
    _description = 'Data Master Ceisa Cara Angkut'

    _rec_name = 'kode'
    _order = 'kode ASC'

    kode = fields.Char(
        string='Kode Cara Angkut',
        required=True
    )

    name = fields.Char(
        string='Nama Cara Angkut',
        required=True
    )
    
    active = fields.Boolean(
        string='Active',
        default=True
    )

    @api.model
    def create(self, vals_list):
        for vals in vals_list:
            vals['name'] = vals['name'].upper()
        return super(CeisaMasterCaraAngkut, self).create(vals_list)

    
