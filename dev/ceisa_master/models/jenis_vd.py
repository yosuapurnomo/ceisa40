
# -*- coding: utf-8 -*-
###############################################################################
#    License, author and contributors information in:                         #
#    __manifest__.py file at the root folder of this module.                  #
###############################################################################

from odoo import models, fields, api, _
from odoo.exceptions import UserError, ValidationError


class CeisaMasterVD(models.Model):
    _name = 'ceisa_master.vd'
    _description = 'Data Master Ceisa Jenis Voluntary Declaration (VD)'

    _rec_name = 'kode'
    _order = 'kode ASC'
    
    kode = fields.Char(
        string='Kode Jenis Voluntary Declaration (VD)',
        required=True
    )
    
    name = fields.Char(
        string='Nama Jenis Voluntary Declaration (VD)',
        required=True,
    )

    active = fields.Boolean(
        string='Active',
        default=True
    )

    @api.model
    def create(self, vals_list):
        for vals in vals_list:
            vals['kode'] = vals['kode'].upper()
        return super(CeisaMasterVD, self).create(vals_list)