
# -*- coding: utf-8 -*-
###############################################################################
#    License, author and contributors information in:                         #
#    __manifest__.py file at the root folder of this module.                  #
###############################################################################

from odoo import models, fields, api, _
from odoo.exceptions import UserError, ValidationError


class CeisaMasterIncoterm(models.Model):
    _name = 'ceisa_master.incoterm'
    _description = 'Data Master Ceisa Incoterm'

    _rec_name = 'kode'
    _order = 'kode ASC'
    
    kode = fields.Char(
        string='Kode Incoterm',
        required=True
    )
    
    name = fields.Char(
        string='Nama Incoterm',
        required=True,
    )

    active = fields.Boolean(
        string='Active',
        default=True
    )

    @api.model
    def create(self, vals_list):
        for vals in vals_list:
            vals['kode'] = vals['kode'].upper()
        return super(CeisaMasterIncoterm, self).create(vals_list)