from odoo import api, fields, models


class CeisaMasterAsalBarangFTZ(models.Model):
    _name = 'ceisa_master.asal_barang_ftz'
    _description = 'Data Master Ceisa Asal Barang FTZ'

    _rec_name = 'kode'
    _order = 'kode ASC'

    kode = fields.Char(
        string='Kode Asal Barang',
        required=True,
    )
    name = fields.Char(
        string='Nama Asal Barang FTZ',
        required=True
    )

    active = fields.Boolean(
        string='Active',
        default=True
    )
    
