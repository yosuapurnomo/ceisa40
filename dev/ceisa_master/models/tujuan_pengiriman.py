
# -*- coding: utf-8 -*-
###############################################################################
#    License, author and contributors information in:                         #
#    __manifest__.py file at the root folder of this module.                  #
###############################################################################

from odoo import models, fields, api, _
from odoo.exceptions import UserError, ValidationError


class CeisaMasterTujuanPengiriman(models.Model):
    _name = 'ceisa_master.tujuan_pengiriman'
    _description = 'Data Master Ceisa Tujuan Pengiriman'

    _rec_name = 'kode'
    _order = 'kode ASC'

    
    dokumen_id = fields.Many2one(
        string='Kode Dokumen',
        comodel_name='ceisa_master.dokumen',
        ondelete='cascade',
    )
    
    kode = fields.Char(
        string='Kode Tujuan Pengiriman',
        required=True
    )
    
    name = fields.Char(
        string='Nama Tujuan Pengiriman',
        required=True,
    )

    active = fields.Boolean(
        string='Active',
        default=True
    )

    @api.model
    def create(self, vals_list):
        for vals in vals_list:
            vals['name'] = vals['name'].upper()
        return super(CeisaMasterTujuanPengiriman, self).create(vals_list)