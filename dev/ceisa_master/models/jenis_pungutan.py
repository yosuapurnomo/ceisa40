
# -*- coding: utf-8 -*-
###############################################################################
#    License, author and contributors information in:                         #
#    __manifest__.py file at the root folder of this module.                  #
###############################################################################

from odoo import models, fields, api, _
from odoo.exceptions import UserError, ValidationError


class CeisaMasterPungutan(models.Model):
    _name = 'ceisa_master.pungutan'
    _description = 'Data Master Ceisa Jenis Pungutan'

    _rec_name = 'kode'
    _order = 'kode ASC'
    
    kode = fields.Char(
        string='Kode Jenis Pungutan',
        required=True
    )
    
    name = fields.Char(
        string='Nama Jenis Pungutan',
        required=True,
    )

    active = fields.Boolean(
        string='Active',
        default=True
    )

    @api.model
    def create(self, vals_list):
        for vals in vals_list:
            vals['kode'] = vals['kode'].upper()
        return super(CeisaMasterPungutan, self).create(vals_list)